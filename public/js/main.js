const leagues = d3.json('https://api.pathofexile.com/leagues?type=main&compact=1&realm=pc')
    .then((data) => {
        return data.sort((a, b) => {
            if (a.endAt && !b.endAt) return -1;
            if (!a.endAt && b.endAt) return 1;
            return 0;
        })
        .reduce((ls, datum) => {
            if (!/^SSF/.test(datum.id)) ls.push(datum.id);
            return ls;
        }, []);
    });

const biomes = [
    {
        name: 'Mines',
        min: 0,
        max: 64
    },
    {
        name: 'Fungal Caverns',
        min: 5,
        max: Infinity
    },
    {
        name: 'Petrified Forest',
        min: 5,
        max: Infinity
    },
    {
        name: 'Abyssal Depths',
        min: 11,
        max: Infinity
    },
    {
        name: 'Frozen Hollow',
        min: 21,
        max: Infinity
    },
    {
        name: 'Magma Fissure',
        min: 31,
        max: Infinity
    },
    {
        name: 'Sulfur Vents',
        min: 56,
        max: Infinity
    },
];

let fossils = [
    {
        name: 'Aberrant Fossil',
        value: {
            single: undefined,
            bulk: undefined
        },
        biomes: [1, 3],
        wall: false,
        node: false,
        selected: false,
        price: undefined,
        unit: undefined,
        mods: [
            'More Chaos modifiers',
            'No Lightning modifiers'
        ]
    },
    {
        name: 'Aetheric Fossil',
        value: {
            single: undefined,
            bulk: undefined
        },
        biomes: [0, 6],
        wall: false,
        node: false,
        selected: false,
        price: undefined,
        unit: undefined,
        mods: [
            'More Caster modifiers',
            'Fewer Attack modifiers'
        ]
    },
    {
        name: 'Bloodstained Fossil',
        value: {
            single: undefined,
            bulk: undefined
        },
        biomes: [],
        wall: false,
        node: true,
        selected: false,
        price: undefined,
        unit: undefined,
        mods: [
            'Has a Vaal modifier'
        ]
    },
    {
        name: 'Bound Fossil',
        value: {
            single: undefined,
            bulk: undefined
        },
        biomes: [2, 3],
        wall: false,
        node: false,
        selected: false,
        price: undefined,
        unit: undefined,
        mods: [
            'More Minion or Aura modifiers'
        ]
    },
    {
        name: 'Corroded Fossil',
        value: {
            single: undefined,
            bulk: undefined
        },
        biomes: [1, 2, 3],
        wall: false,
        node: false,
        selected: false,
        price: undefined,
        unit: undefined,
        mods: [
            'More Poison or Bleeding modifiers',
            'No Elemental modifiers'
        ]
    },
    {
        name: 'Dense Fossil',
        value: {
            single: undefined,
            bulk: undefined
        },
        biomes: [1, 2],
        wall: false,
        node: false,
        selected: false,
        price: undefined,
        unit: undefined,
        mods: [
            'More Armour, Energy Shield or Evasion modifiers',
'            No Life Modifiers'
        ]
    },
    {
        name: 'Enchanted Fossil',
        value: {
            single: undefined,
            bulk: undefined
        },
        biomes: [5],
        wall: true,
        node: false,
        selected: false,
        price: undefined,
        unit: undefined,
        mods: [
            'Has a Labyrinth Enchantment',
            '(Only usable on Helmets, Boots and Gloves)'
        ]
    },
    {
        name: 'Encrusted Fossil',
        value: {
            single: undefined,
            bulk: undefined
        },
        biomes: [5, 6],
        wall: true,
        node: false,
        selected: false,
        price: undefined,
        unit: undefined,
        mods: [
            'More sockets',
            'Can have white sockets'
        ]
    },
    {
        name: 'Faceted Fossil',
        value: {
            single: undefined,
            bulk: undefined
        },
        biomes: [],
        wall: false,
        node: true,
        selected: false,
        price: undefined,
        unit: undefined,
        mods: [
            'More Socketed Gem Level modifiers'
        ]
    },
    {
        name: 'Fractured Fossil',
        value: {
            single: undefined,
            bulk: undefined
        },
        biomes: [],
        wall: false,
        node: true,
        selected: false,
        price: undefined,
        unit: undefined,
        mods: [
            'Creates a mirrored copy'
        ]
    },
    {
        name: 'Frigid Fossil',
        value: {
            single: undefined,
            bulk: undefined
        },
        biomes: [4],
        wall: false,
        node: false,
        selected: false,
        price: undefined,
        unit: undefined,
        mods: [
            'More Cold modifiers',
            'No Fire modifiers'
        ]
    },
    {
        name: 'Gilded Fossil',
        value: {
            single: undefined,
            bulk: undefined
        },
        biomes: [1, 3],
        wall: true,
        node: false,
        selected: false,
        price: undefined,
        unit: undefined,
        mods: [
            'Item is overvalued by vendors',
            '(Adds useless Implicit modifier which increases the odds of a favorable Corruption)'
        ]
    },
    {
        name: 'Glyphic Fossil',
        value: {
            single: undefined,
            bulk: undefined
        },
        biomes: [],
        wall: false,
        node: true,
        selected: false,
        price: undefined,
        unit: undefined,
        mods: [
            'Has a Corrupt Essence modifier'
        ]
    },
    {
        name: 'Hollow Fossil',
        value: {
            single: undefined,
            bulk: undefined
        },
        biomes: [],
        wall: false,
        node: true,
        selected: false,
        price: undefined,
        unit: undefined,
        mods: [
            'Has an Abyssal socket'
        ]
    },
    {
        name: 'Jagged Fossil',
        value: {
            single: undefined,
            bulk: undefined
        },
        biomes: [2],
        wall: false,
        node: false,
        selected: false,
        price: undefined,
        unit: undefined,
        mods: [
            'More Physical modifiers',
            'No Chaos modifiers'
        ]
    },
    {
        name: 'Lucent Fossil',
        value: {
            single: undefined,
            bulk: undefined
        },
        biomes: [3],
        wall: false,
        node: false,
        selected: false,
        price: undefined,
        unit: undefined,
        mods: [
            'More Mana modifiers',
            'No Speed modifiers'
        ]
    },
    {
        name: 'Metallic Fossil',
        value: {
            single: undefined,
            bulk: undefined
        },
        biomes: [0, 6],
        wall: false,
        node: false,
        selected: false,
        price: undefined,
        unit: undefined,
        mods: [
            'More Lightning modifiers',
            'No Physical modifiers'
        ]
    },
    {
        name: 'Perfect Fossil',
        value: {
            single: undefined,
            bulk: undefined
        },
        biomes: [1, 6],
        wall: false,
        node: false,
        selected: false,
        price: undefined,
        unit: undefined,
        mods: [
            'Improved Quality',
            '(15% - 30%)'
        ]
    },
    {
        name: 'Prismatic Fossil',
        value: {
            single: undefined,
            bulk: undefined
        },
        biomes: [4, 5],
        wall: false,
        node: false,
        selected: false,
        price: undefined,
        unit: undefined,
        mods: [
            'More Elemental modifiers',
            'No Poison or Bleeding modifiers'
        ]
    },
    {
        name: 'Pristine Fossil',
        value: {
            single: undefined,
            bulk: undefined
        },
        biomes: [0, 4, 5],
        wall: false,
        node: false,
        selected: false,
        price: undefined,
        unit: undefined,
        mods: [
            'More Life modifiers',
            'No Armour, Energy Shield or Evasion modifiers'
        ]
    },
    {
        name: 'Sanctified Fossil',
        value: {
            single: undefined,
            bulk: undefined
        },
        biomes: [2, 4],
        wall: true,
        node: false,
        selected: false,
        price: undefined,
        unit: undefined,
        mods: [
            'Numeric modifier values are lucky',
            'High Level modifiers are more common'
        ]
    },
    {
        name: 'Scorched Fossil',
        value: {
            single: undefined,
            bulk: undefined
        },
        biomes: [5],
        wall: false,
        node: false,
        selected: false,
        price: undefined,
        unit: undefined,
        mods: [
            'More Fire modifiers',
            'No Cold modifiers'
        ]
    },
    {
        name: 'Serrated Fossil',
        value: {
            single: undefined,
            bulk: undefined
        },
        biomes: [0, 4],
        wall: false,
        node: false,
        selected: false,
        price: undefined,
        unit: undefined,
        mods: [
            'More Attack modifiers',
            'Fewer Caster modifiers'
        ]
    },
    {
        name: 'Shuddering Fossil',
        value: {
            single: undefined,
            bulk: undefined
        },
        biomes: [4, 6],
        wall: true,
        node: false,
        selected: false,
        price: undefined,
        unit: undefined,
        mods: [
            'More Speed modifiers',
            'No Mana modifiers'
        ]
    },
    {
        name: 'Tangled Fossil',
        value: {
            single: undefined,
            bulk: undefined
        },
        biomes: [],
        wall: false,
        node: true,
        selected: false,
        price: undefined,
        unit: undefined,
        mods: [
            'Can have any Fossil modifiers'
        ]
    }
];

/** Returns image URL from the Title Case item name */
function getItemImage (item) {
    return './images/'.concat(item.replace(' ', '_'), '_inventory_icon.png');
}

/** Get fossil prices and update the fossil record with appropriate denominations */
function getPriceData (league) {
    return d3.json(`https://cors-anywhere.herokuapp.com/https://poe.ninja/api/data/itemoverview?league=${league}&type=Fossil`)
    .then((data) => {
        // sort the fossils alphabetically
        data = data.lines.sort((a, b) => {
            if (a.name < b.name) return -1;
            if (a.name > b.name) return 1;
            return 0;
        });
        // Update tables with appropriate price point
        data.forEach((f, i) => {
            const fossilLabelCell = fossils[i].node? d3.select(`#fossil-${i}`) : d3.select(`#fossil-${i}`).select('td:first-of-type');
            if (f.exaltedValue >= 0.8) {
                fossilLabelCell.select('.price')
                    .html(currencyRound(f.exaltedValue));
                fossilLabelCell.select('img.currency')
                    .attr('src', getItemImage('Exalted Orb'))
                    .attr('title', 'Exalted Orb');
            }
            else {
                fossilLabelCell.select('.price')
                    .html(currencyRound(f.chaosValue));
                fossilLabelCell.select('img.currency')
                    .attr('src', getItemImage('Chaos Orb'))
                    .attr('title', 'Chaos Orb');
            }
        });
    })
    .catch((error) => console.error(error));
}

/** Calculate biome farming */
function updateBiomes () {
    let potential = [];
    let map = [];
    biomes.forEach(() => {
        potential.push(0);
        map.push(new Array(fossils.length).fill(0));
    });
    let count = 0;
    fossils.forEach((f, i) => {
        if (f.selected) {
            count ++;
            f.biomes.forEach((j) => {
                potential[j] ++;
                map[j][i] ++;
            });
        }
    });
    const max = Math.max(...potential);
    if (count) {
        if (count === max) {
            potential.forEach((n, i) => {
                const column = d3.selectAll(`.biome-${i}`);
                if (!n) column.classed('best good okay', false).classed('bad', true);
                else if (n < max) column.classed('best good bad', false).classed('okay', true);
                else column.classed('good okay bad', false).classed('best', true);
            });
        }
        else {
            potential.forEach((n, i) => {
                if (n === max) {
                    map[i].forEach((o, j) => {
                        if (o) map.forEach((p) => p[j] = 0);
                    });
                }
            });
            potential.forEach((n, i) => {
                const column = d3.selectAll(`.biome-${i}`);
                if (!n) column.classed('best good okay', false).classed('bad', true);
                else if (n === max) column.classed('good okay bad', false).classed('best', true);
                else if ((map[i].reduce((t, o) => t + o, 0))) column.classed('best okay bad', false).classed('good', true);
                else column.classed('best good bad', false).classed('okay', true);
            });
        }
    }
    else {
        d3.selectAll('#normal-fossils *').classed('best good okay bad', false);
    }
}

/** Appends an element with a fossil image */
function appendFossil (element, fossil, dir) {
    element.append('img')
        .attr('src', getItemImage(fossil.name));
    const tooltip = element.append('div');
    tooltip.classed('tooltip', true)
        .classed(dir, true);
    tooltip.append('div')
        .classed('item-name', true)
        .html(fossil.name);
    const mods = tooltip.append('div')
    mods.classed('modifiers', true);
    fossil.mods.forEach((m) => mods.append('div').html(m));
}

/** Appends an element with a currency image */
function appendCurrency (element, orb) {
    element.append('img')
        .attr('src', getItemImage(orb))
        .attr('title', orb)
        .classed('currency', true);
}

/** Sensibly round currency */
function currencyRound (value) {
    const num = new Number(value);
    if (num < 10) return num.toFixed(1).replace(/\.0$/,'');
    return Math.round(num);
}

document.addEventListener('DOMContentLoaded', (e) => {

    const requestUrl = new URL(window.location.href);
    const path = requestUrl.pathname;

    // save elements
    const leagueSelector = d3.select('#league');
    const normalTable = d3.select('#normal-fossils');
    const nodeTable = d3.select('#node-fossils');

    // Build the tables
    const nameRow = normalTable.append('tr');
    const depthRow = normalTable.append('tr');
    nameRow.classed('biome-name', true);
    depthRow.classed('depth', true);
    nameRow.append('td');
    depthRow.append('td');
    biomes.forEach((b, i) => {
        nameRow.append('td')
            .classed(`biome-${i}`, true)
            .html(`${b.name}`);
        depthRow.append('td')
            .classed(`biome-${i}`, true)
            .html((b.max === Infinity)? `${b.min} +` : `${b.min} - ${b.max}`);
    });
    fossils.forEach((f, i) => {
        // add to node-onlies
        if (f.node) {
            const cell = nodeTable.append('div');
            cell.classed(`fossil-head`, true);
            cell.classed('fossil', true);
            cell.attr('id', `fossil-${i}`)
            appendFossil(cell, f, 'left');
            cell.append('span')
                .classed('price', true);
            cell.select('.price')
                .html('???');
            appendCurrency(cell, 'Chaos Orb');
        }
        // add to the main biome table
        else {
            const row = normalTable.append('tr');
            const firstCell = row.append('td');
            row.classed(`row-option`, true);
            row.attr('id', `fossil-${i}`);
            firstCell.classed('fossil-head', true);
            firstCell.append('span')
                .html(f.name.replace(' Fossil', ''));
            firstCell.append('span')
                .classed('price', true);
            firstCell.select('.price')
                .html('???');
            appendCurrency(firstCell, 'Chaos Orb');
            biomes.forEach((b, j) => {
                const newCell = row.append('td')
                    .classed(`biome-${j}`, true)
                    .classed('fossil', true);
                const newDiv = newCell.append('div');
                // place fossil if it can be found in the biome
                if (f.biomes.includes(j)) {
                    appendFossil(newDiv, f, j < 4? 'right' : 'left');
                    if (f.wall) {
                        newDiv.append('img')
                            .classed('note', true)
                            .attr('src', getItemImage('Fractured Wall'))
                            .attr('title', 'Only behind Fractured Walls');
                    }
                }
            });
            // add farming calculator
            row.on('click', () => {
                f.selected = !f.selected;
                row.classed('selected', f.selected);
                updateBiomes();
            });
        }
    });

    // Build the dropdown
    leagues.then((ls) => {
        ls.forEach((l) => {
            leagueSelector.append('option')
                .attr('value', l)
                .html(l);
        });
        leagueSelector.property('value', requestUrl.searchParams.get('league')? requestUrl.searchParams.get('league') : ls[0]);
        getPriceData(leagueSelector.property('value'));
    });
     
    leagueSelector.on('change', () => {
        let league = leagueSelector.property('value');
        window.history.pushState(null, 'Fossil Cheatsheet', `${path}?league=${league}`);
        getPriceData(league);
    });

    d3.select('.year').html(new Date().getFullYear());

});
